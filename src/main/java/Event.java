import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;


public class Event  implements Comparable<Event>{

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startingTime;

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishingTime;

    private String name;


    public  Event (Date startingTime , Date finishingTime , String name){
        this.startingTime = startingTime;
        this.finishingTime = finishingTime;
        this.name = name;
    }

    public Date getStartingTime(){
        return this.startingTime;
    }

    public Date getFinishingTime() {
        return this.finishingTime;
    }

    public String getName () {
        return this.name;
    }

    @SuppressWarnings("deprecation")
    public int getDate() {
        return this.startingTime.getDate();
    }
    @SuppressWarnings("deprecation")
    public int getMonth() {
        return  this.startingTime.getMonth();
    }


    public int compareTo(Event event){
        int ok = 0;
        if(this.startingTime.before(event.getStartingTime()))
            ok=1;
        else
        if(this.startingTime.after(event.getStartingTime()))
            ok=-1;
        return ok;
    }

    @Override
    public String toString() {
        return "{" +
                "\"startingTime\":" + "\"" + startingTime.toInstant().toEpochMilli() + "\"" +
                ", \"finishingTime\":" + "\"" + finishingTime.toInstant().toEpochMilli() + "\"" +
                ", \"name\":" + "\"" + name + "\"" +
                '}';
    }
}