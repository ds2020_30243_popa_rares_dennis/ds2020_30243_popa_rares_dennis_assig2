import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Send {

    private final static String QUEUE_NAME = "sensor.readings.queue";

    private final static String HOST = "roedeer.rmq.cloudamqp.com";
    private final static String USER = "hpcgspve";
    private final static String PASSWORD = "OfuEW0gNpXucJwjdmLmJa6qqgoAgvImU";
    private final static String VHOST = "hpcgspve";

    public static void main(String[] argv) {
        EventReader reader = new EventReader();
        ArrayList<Event> events = reader.getEvents();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);
        factory.setUsername(USER);
        factory.setPassword(PASSWORD);
        factory.setVirtualHost(VHOST);

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            channel.queuePurge(QUEUE_NAME);
            while(!events.isEmpty()) {
                Event current_event = events.get(0);
                try {
                    channel.basicPublish("", QUEUE_NAME, null, current_event.toString().getBytes());
                    System.out.println(current_event + " " + events.size());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                TimeUnit.SECONDS.sleep(1);
                events.remove(current_event);
            }

        } catch (IOException | TimeoutException | InterruptedException e) {
            e.printStackTrace();
        }


    }
}
