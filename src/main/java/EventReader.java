import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EventReader {

    private ArrayList<Event> events = new ArrayList<Event>();
    private static final String fileName = "activity.txt";

    public EventReader (){
        this.createList();
    }

    public void createList() {
        try {
            Stream<String> stream = Files.lines(Paths.get(EventReader.fileName));
            List<String> allEvents = (List<String>) stream.map(String::toLowerCase).collect(Collectors.toList());
            SimpleDateFormat pattern = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for(String s : allEvents) {
                String[] details = s.split("\\s+");
                String startingTime = details[0] + " " + details[1];
                String finishingTime = details[2] + " " + details[3];
                String name = details[4];
                this.events.add(new Event(pattern.parse(startingTime),pattern.parse(finishingTime), name));
            }
            stream.close();
        }
        catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
        catch(ParseException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public ArrayList<Event> getEvents() {
        return events;
    }

}
